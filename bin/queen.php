<?php

    if (!class_exists('\Symfony\Component\Console\Application')) {
        if (file_exists($file = __DIR__.'/../../../autoload.php') || file_exists($file = __DIR__.'/../autoload.php') || file_exists($file = __DIR__.'/../vendor/autoload.php')) {
            require_once $file;
        } elseif (file_exists($file = __DIR__.'/../autoload.php.dist')) {
            require_once $file;
        }
    }

    use AntFarm\AntFarm\AntFarm;
    use Symfony\Component\Console\Application;
    use Symfony\Component\Finder\Finder;

    $finder = new Finder();
    $finder->files()->name('*.php')->in(__DIR__.'/../src/AntFarm/AntFarm/Command')->depth(0);

    $app = new Application('Queen', AntFarm::VERSION);

    foreach ($finder as $file) {
        $ns = '\\AntFarm\\AntFarm\\Command';
        $r  = new \ReflectionClass($ns.'\\'.$file->getBasename('.php'));
        if ($r->isSubclassOf('Symfony\\Component\\Console\\Command\\Command') && !$r->isAbstract()) {
            $app->add($r->newInstance());
        }
    }

    $app->run();
