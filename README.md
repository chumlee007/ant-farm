## Software Requirements
* LAMP Stack or Linux, Nginx, Php, HHVM, MySql
* Redis w/phpredis extension

## Installation
* Make a local copy of htaccess.sample and then rename that copy to .htaccess
* Run `composer install`
* Run `vendor/bin/queen init`
* Change all configuration settings found in `.env` to match the environment

After installation is complete make sure to add the following to your composer file

```
"autoload": {
	"classmap": [
		"app/controllers",
		"app/middleware",
		"app/models"
	]
}
```

Then run `composer autoload`

## Sessions
Sessions by default use PHP's native session handler.

However this can be configured to use either of the following in-memomry caching systems:
* Memcached
* Redis

AntFarm can be configured to use any one of these stores by changing the configuartion settings found in the .env file. Simply change the `SESSION_DRIVER` to `memcached` or `redis`. The deafult session driver value is `native`.

Please note that you will have to install the necessary software on your server to support Memcached or Redis as well as their accompanying PHP extensions [memcached](http://php.net/manual/en/book.memcached.php) or [phpredis](https://github.com/phpredis/phpredis)

### Environments
The application environment is configured within the `.env` file.

#### Local, Development, Qa, Staging and Production
Environment names are typically: local, development, qa, staging and production

## Database Configuration
Once all database configurations have been set in the .env file, do the following:
* Run `php propel --config-dir=config --schema-dir=database --output-dir=database/sql sql:build`
* Run `php propel --config-dir=config --output-dir=app/models model:build`
* Run `php propel --config-dir=config --output-dir=database/config config:convert`
* Run `php propel --config-dir=config sql:insert`
* Run `composer dump-autoload`

## Database Migrations
### Creating migrations
* Run `php propel --config-dir=config --schema-dir=database --output-dir=database/migrations diff`

### Running migrations
* Run `php propel --config-dir=config --output-dir=database/migrations migrate`
* Run `php propel --config-dir=config --schema-dir=database --output-dir=database/sql sql:build`
* Run `php propel --config-dir=config --output-dir=app/models model:build`
* Run `composer dump-autoload`

See www.propelorm.org for more details
