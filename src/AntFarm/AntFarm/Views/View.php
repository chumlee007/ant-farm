<?php

namespace AntFarm\AntFarm\Views;

use AntFarm\AntFarm\AntFarm;

class View
{
	private $app;
	private $currentPath;
	private $currentData;
	private $currentTemplate;

	public function __construct(AntFarm $app) 
	{
		$this->app = $app;
	}
	
	private function dumpView()
	{
		$html = file_get_contents($this->currentTemplate);

		ob_start();

        if ($this->currentData) extract($this->currentData);

		try
		{
			include $this->currentTemplate;
		} catch (\Exception $e)
		{
			ob_get_clean(); throw $e;
		}

		$this->contents = ltrim(ob_get_clean());

		if (\App::inDebugMode()):
			$js = '<script>var antFarmDebugWindows=document.querySelectorAll(".antfarm-debug-window");if(antFarmDebugWindows)for(var w=0;w<antFarmDebugWindows.length;w++){var dWindow=antFarmDebugWindows[w],antfarmShowMore=dWindow.querySelector(".antfarm-show-more");antfarmShowMore.addEventListener("click",function(e){e.preventDefault();var n=e.target,a=n.dataset.debugDetailsId,t=document.querySelector("#"+a);t&&("none"===t.style.display?(t.style.display="block",n.innerText="less"):(t.style.display="none",n.innerText="more"))},!1)}</script>';

			$this->contents = str_replace('</body>', $js, $this->contents);
		endif;

		return $this;
	}

	/**
	 * Render a view 
	 * @param string $path Path to template
	 * @param object $data Variables to be passed to the view
	 * @return string Compiled view
	 */
	public function render($path, $data = array(), $absolutePath = FALSE) 
	{
		$this->currentPath = $path;
		$this->currentData = $data;

		if (!$absolutePath):
			$viewsPath = isset($this->app['env']['viewsPath']) ? $this->app['env']['viewsPath'] : '' ;
		else:
			$viewsPath = '';
		endif;

		if (is_array($viewsPath)):
			$found = FALSE;

			foreach($viewsPath as $filePath):
				$this->currentTemplate = $filePath.$this->currentPath;

				if (is_file($this->currentTemplate)):
					$found = TRUE;
					break;
				endif;
			endforeach;

			if (!$found): // try for php
				foreach($viewsPath as $filePath):
					$this->currentTemplate = $filePath.$this->currentPath.'.php';
	
					if (is_file($this->currentTemplate)):
						$found = TRUE;
						break;
					endif;
				endforeach;
			endif;

			if (!$found): // try for html
				foreach($viewsPath as $filePath):
					$this->currentTemplate = $filePath.$this->currentPath.'.html';
	
					if (is_file($this->currentTemplate)):
						$found = TRUE;
						break;
					endif;
				endforeach;
			endif;

			if (!$found):
				throw new \RuntimeException("View cannot locate '$path'. Please verify that the path is correct.");
			endif;
		else:
			$this->currentTemplate = $viewsPath.$this->currentPath;

			if (!is_file($this->currentTemplate)) $this->currentTemplate = $viewsPath.$this->currentPath.'.php';
			if (!is_file($this->currentTemplate)) $this->currentTemplate = $viewsPath.$this->currentPath.'.html';
		endif;

        if (!is_file($this->currentTemplate))
            throw new \RuntimeException("View cannot locate '$this->currentTemplate'. Please verify that the path is correct.");

        return $this->dumpView();
	}

    public function doesViewExist($path, $absolutePath = FALSE)
    {
        $found = FALSE;

        if (!$absolutePath):
            $viewsPath = isset($this->app['env']['viewsPath']) ? $this->app['env']['viewsPath'] : '' ;
        else:
            $viewsPath = '';
        endif;

        if (is_array($viewsPath)):
            foreach($viewsPath as $filePath):
                $template = $filePath.$path;

                if (is_file($template)):
                    $found = TRUE;
                    break;
                endif;
			endforeach;
			
			if (!$found):
				foreach($viewsPath as $filePath): // try for php
					$template = $filePath.$path.'.php';

					if (is_file($template)):
						$found = TRUE;
						break;
					endif;
				endforeach;
			endif;
			
			if (!$found):
				foreach($viewsPath as $filePath): // try for html
					$template = $filePath.$path.'.php';

					if (is_file($template)):
						$found = TRUE;
						break;
					endif;
				endforeach;
			endif;
        else:
			$found = is_file($viewsPath.$path);
			
			if (!$found && is_file($viewsPath.$path)) $found = is_file($viewsPath.$path.'.php');
			if (!$found && is_file($viewsPath.$path.'.php')) $found = is_file($viewsPath.$path.'.html');
        endif;

        return $found;
    }

	public function __toString()
	{
		return $this->contents;
	}
}