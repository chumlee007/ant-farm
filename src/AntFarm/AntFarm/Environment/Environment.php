<?php

namespace AntFarm\AntFarm\Environment;

use AntFarm\AntFarm\Helper;

class Environment implements \ArrayAccess, \IteratorAggregate
{
	public $props = array();
	
	public function __construct() 
	{
		$this->props['phpVersion'] = phpversion();
		$this->props['serverSoftware'] = (php_sapi_name() !== 'cli') ? $_SERVER['SERVER_SOFTWARE'] : '';
		$this->props['serverName'] = (php_sapi_name() !== 'cli') ? $_SERVER['SERVER_NAME'] : '';
		$this->props['port'] = (php_sapi_name() !== 'cli') ? $_SERVER['SERVER_PORT'] : '';
		$this->props['ipAddress'] = (php_sapi_name() !== 'cli') ? Helper::getClientIp() : '';
		$this->props['requestMethod'] = (php_sapi_name() !== 'cli') ? $_SERVER['REQUEST_METHOD'] : '';
		$this->props['requestUri'] = (php_sapi_name() !== 'cli') ? $_SERVER['REQUEST_URI'] : '';
		$this->props['httpHost'] = (php_sapi_name() !== 'cli') ? $_SERVER['HTTP_HOST'] : '';
		$this->props['httpOrigin'] = (php_sapi_name() !== 'cli') ? isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN'] : '' : '';
		$this->props['referrerUrl'] = (php_sapi_name() !== 'cli') ? isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' : '';
		$this->props['ajaxRequest'] = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? 1 :  0;
		$this->props['contentType'] = (!empty($_SERVER['CONTENT_TYPE'])) ? $_SERVER['CONTENT_TYPE'] : '';
		$this->props['userAgent'] = (php_sapi_name() !== 'cli') ? $_SERVER['HTTP_USER_AGENT'] : '';
		$this->props['script'] = (php_sapi_name() !== 'cli') ? $_SERVER['SCRIPT_FILENAME'] : '';
	}
	
	public function offsetSet($offset, $value) 
	{
		if (is_null($offset)):
			$this->props[] = $value;
		else:
			$this->props[$offset] = $value;
		endif;
	}
	
	public function offsetExists($offset) 
	{
		return isset($this->props[$offset]);
	}
	
	public function offsetUnset($offset) 
	{
		unset($this->props[$offset]);
	}

	public function offsetGet($offset) 
	{
		return isset($this->props[$offset]) ? $this->props[$offset] : null;
	}
	
	public function getIterator() 
	{
		return new ArrayIterator($this->props);
	}
}
