<?php
namespace AntFarm\AntFarm;

use AntFarm\AntFarm\ServiceContainer\ServiceFactory;
use AntFarm\AntFarm\Helper;
use AntFarm\AntFarm\Cache\SharedMemory\SharedMemory;

class AntFarm implements \ArrayAccess, \IteratorAggregate
{
	const VERSION = '1.1.2';

	public static $props = array();
	public $env;

	private static $config;
	private static $session;
    private static $cache;
    private static $minify;
    private static $mail;
	private static $input;

	private $view;

	private $baseUrl;
	private $environment;
	private $router;
	private $whoops;

	/**
	 * Application constructor
	 * @param array $config Configuration array valid keys are baseUrl, viewsPath
	 */
	public function __construct($config)
	{
		if (!isset($config['baseUrl']) && !array_key_exists('baseUrl', $config)) throw new \Exception("Missing baseUrl from configuration array");

		if (!isset($config['environment']) && !array_key_exists('environment', $config)) throw new \Exception("Missing environment from configuration array");

		static::$config = $config;

		if (!empty($config['timezone'])) date_default_timezone_set($config['timezone']);

		$this->environment = $config['environment'];

		$this->registerServices();
		$this->startWhoops();

		if (!empty($config['timezone'])):
			$dbHost = getenv('DB_HOST');
			$dbName = getenv('DB_DATABASE');
			$dbUser = getenv('DB_USERNAME');
			$dbPassword = getenv('DB_PASSWORD');

			if ($dbHost && $dbName && $dbUser): // sync mysql DB 
				$now = new \DateTime();
				$mins = $now->getOffset() / 60;
				$sgn = ($mins < 0 ? -1 : 1);
				$mins = abs($mins);
				$hrs = floor($mins / 60);
				$mins -= $hrs * 60;
				$offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);

				$db = new \PDO('mysql:host='.$dbHost.';dbname='.$dbName, $dbUser, $dbPassword);
				$db->exec("SET time_zone='$offset';");
			endif;
		endif;
	}

	private function registerServices()
    {
        $serviceFactory = new ServiceFactory($this);

        $this->env = $serviceFactory->environment;
        $this->baseUrl = strtolower(trim(static::$config['baseUrl'], '/'));
        $this->env['baseUrl'] = $this->baseUrl;
		$this->env['environment'] = $this->environment;
		
		$publicFolder = isset(static::$config['publicFolder']) ? static::$config['publicFolder'] : 'public';

        if (isset(static::$config['viewsPath']) && array_key_exists('viewsPath', static::$config)):
            if (!is_array(static::$config['viewsPath'])):
                $this->env['viewsPath'] = realpath(static::$config['viewsPath']);
            else:
                $this->env['viewsPath'] = static::$config['viewsPath'];
            endif;
        endif;

		$this->router = $serviceFactory->router;
		$this->view = $serviceFactory->view;

		static::$props['env'] = $this->env;
		static::$props['router'] = $this->router;
		static::$props['view'] = $this->view;

		static::$props['appPath'] = realpath(__DIR__ . '/../../../../../../app');
		static::$props['basePath'] = realpath(__DIR__ . '/../../../../../../');
		static::$props['publicPath'] = realpath(__DIR__ . '/../../../../../../'.$publicFolder);
		static::$props['storagePath'] = realpath(__DIR__ . '/../../../../../../storage');

		$this->env['appPath'] = static::$props['appPath'];
		$this->env['basePath'] = static::$props['basePath'];
		$this->env['publicPath'] = static::$props['publicPath'];
		$this->env['storagePath'] = static::$props['storagePath'];

		static::$session = $serviceFactory->session;
		static::$props['session'] = static::$session;

		static::$cache = $serviceFactory->cache;
        static::$props['cache'] = static::$cache;

        static::$minify = $serviceFactory->minify;
        static::$props['minify'] = static::$minify;

        static::$mail = $serviceFactory->mail;
        static::$props['mail'] = static::$mail;

		static::$input = $serviceFactory->input;
	}

	private function startWhoops()
	{
		$this->whoops = new \Whoops\Run;

        $debug = filter_var(static::$config['debug'], FILTER_VALIDATE_BOOLEAN);
		
        if ($debug):
            if (\Whoops\Util\Misc::isAjaxRequest()):
                $errorJson = new \Whoops\Handler\JsonResponseHandler();

                $this->whoops->pushHandler($errorJson);
            endif;
        endif;

		// MonoLog
		$logger = new \Monolog\Logger('AntFarm');
		$logFile = static::$props['storagePath'] . '/logs/antfarm.log';

		if (!file_exists($logFile)) touch($logFile);

		$logger->pushHandler(new \Monolog\Handler\StreamHandler($logFile, \Monolog\Logger::WARNING));
		$logger->pushHandler(new \Monolog\Handler\StreamHandler($logFile, \Monolog\Logger::ERROR));
		$logger->pushHandler(new \Monolog\Handler\StreamHandler($logFile, \Monolog\Logger::CRITICAL));
		$logger->pushHandler(new \Monolog\Handler\StreamHandler($logFile, \Monolog\Logger::ALERT));
		$logger->pushHandler(new \Monolog\Handler\StreamHandler($logFile, \Monolog\Logger::INFO));

		$errorLogger = new \Whoops\Handler\PlainTextHandler($logger);

		if (!$debug):
			$errorLogger->loggerOnly(TRUE); // only log error details

			$this->whoops->pushHandler(function($exception, $inspector, $run) { // display custom message
				if (!static::isAjax()):
					$view = static::$props['view'];
					$errorPage = static::getConfigItem('errorPage');

					if ($view->doesViewExist($errorPage)):
						echo $view->render($errorPage)->__toString();
					else:
						echo $view->render(__DIR__.'/ErrorMessages/whoops.php', [], TRUE)->__toString();
					endif;
				elseif(static::isAjax()):
					static::returnJson([
						'code' => 500,
						'message' => "Sorry ... An error occured"]
					,500);
				endif;

				if (static::runningInConsole()) echo 'An error occurred see logs for details'.PHP_EOL;

				return \Whoops\Handler\Handler::DONE;
			});
		endif;

		$this->whoops->pushHandler($errorLogger);

		static::$props['log'] = $logger;

		$this->whoops->register();
	}

	/**
	 * Add a slug prefix to a specific
	 * @param  String $route  Url slug
	 * @param  String $prefix String to prefix route
	 * @return NULL
	 */
	public function prefix($route, $prefix)
	{
		$this->router->addPrefix($route, $prefix);
	}

	/**
	 * Register a route of method type get
	 * @param  String $route  Url slug
	 * @param  Function|string $callable Function to be called when the route is found
	 * @return NULL
	 */
	public function get($route, $callable)
	{
		$route = Helper::isString($route);
		$callable = Helper::isCallable($callable);

		$this->router->addRoute(array(
			'route' => strtolower($route) ,
			'method' => 'get',
			'action' => $callable,
		));
	}

	/**
	 * Register a route of method type delete
	 * @param  String $route  Url slug
	 * @param  Function|string $callable Function to be called when the route is found
	 * @return NULL
	 */
	public function delete($route, $callable)
	{
		$route = Helper::isString($route);
		$callable = Helper::isCallable($callable);

		$this->router->addRoute(array(
			'route' => strtolower($route) ,
			'method' => 'delete',
			'action' => $callable,
		));
	}

	/**
	 * Register a route of method type put
	 * @param  String $route  Url slug
	 * @param  Function|string $callable Function to be called when the route is found
	 * @return NULL
	 */
	public function put($route, $callable)
	{
		$route = Helper::isString($route);
		$callable = Helper::isCallable($callable);

		$this->router->addRoute(array(
			'route' => strtolower($route) ,
			'method' => 'put',
			'action' => $callable,
		));
	}

	/**
	 * Register a route of method type patch
	 * @param  String $route  Url slug
	 * @param  Function|string $callable Function to be called when the route is found
	 * @return NULL
	 */
	public function patch($route, $callable)
	{
		$route = Helper::isString($route);
		$callable = Helper::isCallable($callable);

		$this->router->addRoute(array(
			'route' => strtolower($route) ,
			'method' => 'patch',
			'action' => $callable,
		));
	}

	/**
	 * Register a route of method type post
	 * @param  String $route  Url slug
	 * @param  Function|string $callable Function to be called when the route is found
	 * @return NULL
	 */
	public function post($route, $callable)
	{
		$route = Helper::isString($route);
		$callable = Helper::isCallable($callable);

		$this->router->addRoute(array(
			'route' => strtolower($route) ,
			'method' => 'post',
			'action' => $callable,
		));
	}

	/**
	 * Register a route of method type options
	 * @param  String $route  Url slug
	 * @param  Function|string $callable Function to be called when the route is found
	 * @return NULL
	 */
	public function options($route, $callable)
	{
		$route = Helper::isString($route);
		$callable = Helper::isCallable($callable);

		$this->router->addRoute(array(
			'route' => strtolower($route) ,
			'method' => 'options',
			'action' => $callable,
		));
	}

	/**
	 * Register a route of method type any get|post
	 * @param  String $route  Url slug
	 * @param  Function|string $callable Function to be called when the route is found
	 * @return NULL
	 */
	public function any($route, $callable)
	{
		$route = Helper::isString($route);
		$callable = Helper::isCallable($callable);

		$this->router->addRoute(array(
			'route' => strtolower($route) ,
			'method' => 'any',
			'action' => $callable,
		));
	}

	public function response($httpCode, $callable)
    {
        $httpCode = Helper::isInt($httpCode);
        $callable = Helper::isCallable($callable);

        $this->router->addResponse($httpCode, $callable);
    }

    /**
     * Static cache interface from $app variable
     */
    public static function cache()
    {
        return static::$cache;
    }

    public static function mail()
    {
        return static::$mail;
    }

    /**
     * @param $key Config key
     * @param $value Value to be stored
     */
    public static function setConfigItem($key, $value)
    {
        static::$config[$key] = $value;
    }

    /**
     * Use to get a configuration item from the configuration array
     * @param $key Key to retrieve
     * @return null Returns null if key is not found
     */
    public static function getConfigItem($key)
    {
        if (array_key_exists($key, static::$config)) return static::$config[$key];

        return NULL;
    }

	public static function returnJson($data, $statusCode = 200)
	{
		header('Content-Type: application/json; charset=UTF-8', true, $statusCode);

		echo json_encode($data);

		exit;
	}

	public static function returnView($path, $data = array() , $absolutePath = FALSE)
	{
		$view = static::$props['view'];
		$flashData = static::$session->getAllFlashData();
		$mergedData = ($flashData) ? array_merge($data, $flashData) : $data;

		return $view->render($path, $mergedData, $absolutePath)->__toString();
	}

	public static function renderView($path, $data = array() , $absolutePath = FALSE)
	{
		$view = static::$props['view'];
		$flashData = static::$session->getAllFlashData();
		$mergedData = ($flashData) ? array_merge($data, $flashData) : $data;

		echo $view->render($path, $mergedData, $absolutePath)->__toString();
	}

	public static function hasView($path)
	{
		$view = static::$props['view'];
		return $view->doesViewExist($path);
	}

	public static function log($message)
	{
		static::$props['log']->addInfo($message);
	}

	public static function baseUrl()
	{
        $url = static::$props['env']['baseUrl'];

        if (strpos($url, 'http://') === FALSE && strpos($url, 'https://') === FALSE):
		    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == 443))) ? "https://" : "http://";

		    return $protocol . $url . '/';
        else:
            return $url . '/';
        endif;
	}

	public static function requestPath($url = '')
	{
		return static::$props['router']->getPath($url);
	}

	public static function httpHost()
	{
		return static::$props['env']['httpHost'];
	}

	public static function httpOrigin()
	{
		return static::$props['env']['httpOrigin'];
	}

	public static function requestUri()
	{
		return static::$props['env']['requestUri'];
	}

	public static function referrerUrl()
	{
		return static::$props['env']['referrerUrl'];
	}

	public static function requestMethod()
	{
		return static::$props['env']['requestMethod'];
	}

	public static function contentType()
	{
		return static::$props['env']['contentType'];
	}

	public static function redirect($url, $statusCode = 303)
	{
		$url = static::$props['router']->setPrefix($url);

		header('Location:' . $url, true, $statusCode);

		die();
	}

    public static function forceDownload($path)
    {
        $filePath = $path;

        if (is_file($filePath)):
            // required for IE
            if (ini_get('zlib.output_compression')) ini_set('zlib.output_compression', 'Off');

            // get the file mime type using the file extension
            switch(strtolower(substr(strrchr($filePath,'.'), 1))):
                case 'pdf': $mime = 'application/pdf'; break;
                case 'zip': $mime = 'application/zip'; break;
                case 'jpeg':
                case 'jpg': $mime = 'image/jpg'; break;
                case 'svg':
                case 'svgz': $mime = 'image/svg+xml'; break;
                default: $mime = 'application/force-download';
            endswitch;

            header('Pragma: public'); 	// required
            header('Expires: 0');		// no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime($filePath)).' GMT');
            header('Cache-Control: private',false);
            header('Content-Type: '.$mime);
            header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($filePath));	// provide file size
            header('Connection: close');
            readfile($filePath);		// push it out
            exit();
        endif;
    }

	public static function redirectWithData($url, $data, $statusCode = 303)
	{
		foreach ($data as $key => $value):
			static::$session->flash($key, $value);
		endforeach;

		header('Location:' . $url, true, $statusCode);

		die();
	}

	public static function requestSegments($stripGetVariables = FALSE)
	{
		$baseUrl = static::baseUrl();
		$baseUrlSegments = explode("/", $baseUrl);
		$requestUri = static::requestUri();
		$urlSegments = str_replace($baseUrlSegments, '', $requestUri);

		if ($stripGetVariables):
			$urlSegments = explode('?', $urlSegments);
			$urlSegments = $urlSegments[0];
		endif;

		$segments = explode("/", $urlSegments);
		$clean = array_filter($segments);

		return array_values($clean);
	}

	public static function requestSegment($n)
	{
		$segments = static::requestSegments();

		return $segments[$n];
	}

    public static function generateXsrfObject()
    {
		if (!static::$session->get('_antFarmXsrfToken')):
			$token = Helper::genToken(32);

			static::$session->put('_antFarmXsrfToken', $token);
			setcookie('XSRF-TOKEN', $token);
		else:
			$token = static::$session->get('_antFarmXsrfToken');
		endif;

        return array(
            'key' => '_antFarmXsrfToken',
            'value' => $token
        );
    }

    public static function validateXsrf()
    {
        $cookieToken = isset($_SERVER['HTTP_X_XSRF_TOKEN']) ? $_SERVER['HTTP_X_XSRF_TOKEN'] : NULL ;

        if ($cookieToken):
            $sessToken = static::$session->get('_antFarmXsrfToken');

            if ($cookieToken === $sessToken)
                return TRUE;
        endif;

        return FALSE;
    }

    public static function generateCsrfObject()
	{
		$token = Helper::genToken(32);

		static::$session->put('_antFarmCsrfToken', $token);

		return array(
			'key' => '_antFarmCsrfToken',
			'value' => $token
		);
	}

	public static function generateCsrf()
	{
		$token = Helper::genToken(32);

		static::$session->put('_antFarmCsrfToken', $token);

		return '<input name="_antFarmCsrfToken" type="hidden" value="' . $token . '">';
	}

	public static function validateCsrf()
	{	
		$antFarmToken = static::$input->get('_antFarmCsrfToken');

		if (isset($antFarmToken)):
			$token = static::$session->get('_antFarmCsrfToken');

			if (static::$input->get('_antFarmCsrfToken') === $token):
				static::$session->forget('_antFarmCsrfToken');

				return TRUE;
			endif;
		endif;

		return FALSE;
	}

	public static function runningInConsole()
	{
		return (php_sapi_name() === 'cli');
	}

	public static function inDebugMode()
	{
		return filter_var(static::$config['debug'], FILTER_VALIDATE_BOOLEAN);
	}

	public static function isAjax()
	{
		return static::$props['env']['ajaxRequest'];
	}

	public static function isJSON($string)
    {
        return Helper::isJSON($string);
    }

	public static function getEnvironment()
	{
		return static::$props['env']['environment'];
	}

	public static function appPath()
	{
		return static::$props['appPath'];
	}

	public static function basePath()
	{
		return static::$props['basePath'];
	}

	public static function publicPath()
	{
		return static::$props['publicPath'];
	}

	public static function storagePath()
	{
		return static::$props['storagePath'];
	}

    /**
     * HELPERS
     */

    /**
     * Converts a string to a readable url
     * @param $str String to convert
     * @param array $replace Additional characters to replace
     * @param string $delimiter Default character to be replaced with
     * @return mixed|string
     */
    public static function strtourl($str, $replace = array(), $delimiter='-') {
        setlocale(LC_ALL, 'en_US.UTF8');

        if (!empty($replace))
            $str = str_replace((array)$replace, ' ', $str);

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

    public static function pp($data, $die = FALSE)
    {
        Helper::p($data);

        if ($die) die();
    }

	/**
	 * Start application
	 */
	public function run()
	{
		if (!static::runningInConsole()):
			$result = $this->router->handleRequest();

			if ($result):
				call_user_func_array($result['action'], array_values($this->router->getParams()));
			else:
				http_response_code(404);

				if (!static::isAjax()):
					$view = static::$props['view'];
					$callable = $this->router->getResponse(404);
					$notFoundPage = static::getConfigItem('notFoundPage');

					if (!$callable):
						if ($view->doesViewExist($notFoundPage)):
							echo $view->render($notFoundPage)->__toString();
						else:
							echo $view->render(__DIR__.'/ErrorMessages/404.php', [], TRUE)->__toString();
						endif;
					else:
						call_user_func_array($callable, array_values($this->router->getParams()));
					endif;
				else:
					static::returnJson([
						'code' => 404,
						'message' => "Sorry ... looks like we can't find the page you're looking for"]
					,404);
				endif;
			endif;

			static::$session->flushFlashData();

			return $result;
		endif;
	}

	public function offsetSet($offset, $value)
	{
		if (is_null($offset)):
			static::$props[] = $value;
		else:
			static::$props[$offset] = $value;
		endif;
	}

	public function offsetExists($offset)
	{
		return isset(static::$props[$offset]);
	}

	public function offsetUnset($offset)
	{
		unset(static::$props[$offset]);
	}

	public function offsetGet($offset)
	{
		return isset(static::$props[$offset]) ? static::$props[$offset] : null;
	}

	public function getIterator()
	{
		return new \ArrayIterator(static::$props);
	}
}

class_alias('AntFarm\AntFarm\AntFarm', 'App');
class_alias('AntFarm\AntFarm\Controllers\Controller', 'Controller');
class_alias('AntFarm\AntFarm\Session\Session', 'Session');
class_alias('AntFarm\AntFarm\Router\Input', 'Input');
class_alias('AntFarm\AntFarm\Cache\Cache', 'Cache');
class_alias('AntFarm\AntFarm\Cache\Minify', 'Minify');
class_alias('AntFarm\AntFarm\Helpers\ArrayDotAccess', 'ArrayDotAccess');
class_alias('AntFarm\AntFarm\Helpers\MobileDetect', 'MobileDetect');
class_alias('AntFarm\AntFarm\Helper', 'Queen');
class_alias('PHPMailer\PHPMailer\PHPMailer', 'Mail');