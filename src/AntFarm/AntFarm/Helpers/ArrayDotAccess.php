<?php
    namespace AntFarm\AntFarm\Helpers;

    class ArrayDotAccess implements \ArrayAccess, \IteratorAggregate
    {
        private $data = [];

        public function __construct(array $data)
        {
            $this->data = $data ?: [];
        }

        public function get($key, $default = NULL)
        {
            $currentData = $this->data;

            if (!is_string($key) || empty($key) || !count($currentData))
                return $default;

            // @assert $key contains a dot notated string
            if (strpos($key, '.') !== false):
                $keys = explode('.', $key);

                foreach ($keys as $innerKey):
                    // @assert $data[$innerKey] is available to continue
                    // @otherwise return $default value
                    if (!array_key_exists($innerKey, $currentData))
                        return $default;

                    $currentData = $currentData[$innerKey];
                endforeach;

                return $currentData;
            endif;

            // @fallback returning value of $key in $data or $default value
            return array_key_exists($key, $currentData) ? $currentData[$key] : $default;
        }

        public function has($key)
        {
            $currentData = $this->data;

            if (!is_string($key) || empty($key) || !count($currentData))
                return FALSE;

            // @assert $key contains a dot notated string
            if (strpos($key, '.') !== false):
                $keys = explode('.', $key);

                foreach ($keys as $innerKey):
                    // @assert $data[$innerKey] is available to continue
                    // @otherwise return $default value
                    if (!array_key_exists($innerKey, $currentData))
                        return FALSE;

                    $currentData = $currentData[$innerKey];
                endforeach;

                return TRUE;
            endif;

            // @fallback returning value of $key in $data or $default value
            return array_key_exists($key, $currentData) ? TRUE : FALSE;
        }

        public function offsetSet($offset, $value)
        {
            if (is_null($offset)):
                $this->data[] = $value;
            else:
                $this->data[$offset] = $value;
            endif;
        }

        public function offsetExists($offset)
        {
            return isset($this->data[$offset]);
        }

        public function offsetUnset($offset)
        {
            unset($this->data[$offset]);
        }

        public function offsetGet($offset)
        {
            return isset($this->data[$offset]) ? $this->data[$offset] : null;
        }

        public function getIterator()
        {
            return new \ArrayIterator($this->data);
        }
    }