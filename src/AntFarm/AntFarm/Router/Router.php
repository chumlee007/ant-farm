<?php
// TODO: Add router caching
namespace AntFarm\AntFarm\Router;

use AntFarm\AntFarm\AntFarm;
use AntFarm\AntFarm\Cache\SharedMemory\SharedMemory;

class Router implements \ArrayAccess, \IteratorAggregate
{
	private $routes = array();
    private $response = array();
	private $prefixes = array();
	private $app = NULL;
	private $params = array();

	public function __construct(AntFarm $app)
	{
		$this->app = $app;
	}

	/**
	 * Add a route to the routes array
	 * @param array $route
	 */
	public function addRoute($route)
	{
		array_push($this->routes, $route);
	}

	public function addResponse($httpCode, $callable)
    {
        $this->response[$httpCode] = $callable;
    }

    public function getResponse($httpCode)
    {
        return (isset($this->response[$httpCode])) ? $this->response[$httpCode] : NULL ;
    }

	/**
	 * @param string $route
	 * @param string $prefix
	 */
	public function addPrefix($route, $prefix)
	{
		if (!isset($this->prefixes[$prefix])) $this->prefixes[$prefix] = ' ';

		$this->prefixes[$prefix].= $route . ' ';
	}

	/**
	 * @param string $url
	 * @return string
	 */
	public function setPrefix($url)
	{
		$segments = explode('/', $url);
		$prefix = (count($segments)) ? array_shift($segments) : '';

		if (!empty($prefix)):
			if (isset($this->prefixes[$prefix])):
				return $url;
			else:
				foreach ($this->prefixes as $key => $list):
					if (strpos($list, $url) > - 1):
						return $key . '/' . $url;
						break;
					endif;
				endforeach;
			endif;
		endif;

		return $url;
	}

	/**
	 * @param string $url
	 */
	public function removePrefix($url)
	{
		$segments = explode('/', $url);
		$prefix = (count($segments)) ? array_shift($segments) : '';

		if (!empty($prefix)):
			if (isset($this->prefixes[$prefix])):
				$urlNoPrefix = implode('/', $segments);
				$prefixes = $this->prefixes[$prefix];

				if (strpos($prefixes, $urlNoPrefix) > - 1) return $urlNoPrefix;
			endif;

			return $url;
		endif;

		return $url;
	}

	/**
	 * Handles all requests to app's entry point and routes request to appropriate call
	 * @return array|string
	 */
	public function handleRequest()
	{
		return $this->routeSearch();
	}

	/**
	 * Get url slug from requestUri
	 * @param string $url
	 * @return string
	 */
	public function getPath($url = '')
	{
        $baseUrl = $this->app['env']['baseUrl'];
        $segments = explode('/', $baseUrl);
        $request = (!empty($url)) ? trim($url, '/') : trim($this->app['env']['requestUri'], '/');
        $request = explode('?', $request);
        $request = $request[0];

        foreach ($segments as $segment):
            if (strlen($segment)):
                $search = array(
                    $segment . (($segment == "http:" || $segment === "https:") ? '//' : '/'),
                    $segment
                );

                $request = str_replace($search, '', $request);

                if (empty($request)) $request = '/';
            endif;
        endforeach;

        return $request;
	}

	/**
	 * Search routes based on a provided route
	 * @param  String $route
	 * @param Array $array
	 * @return Array or String
	 */
	private function routeSearch()
	{
		$route = $this->getPath();
		$route = $this->removePrefix($route);
		$array = $this->routes;
		$ajax = $this->app['env']['ajaxRequest'];
		$method = strtolower($this->app['env']['requestMethod']);

		if (is_array($array)):
			foreach ($array as $index => $val):
				if (is_array($val)):

					if (isset($val['route']) && isset($val['method'])):
						if ($val['method'] === $method || $val['method'] === 'any'):
							$reservedRoute = ($val['route'] !== '/') ? ltrim(rtrim($val['route'], '/') , '/') : $val['route'];
							$route = ($route !== '/') ? ltrim(rtrim($route, '/') , '/').'/' : $route;
							$pattern = $this->routePattern($reservedRoute);

							if (preg_match($pattern, $route, $params)):
								array_shift($params);
								$this->params = $params;
								return $val;
							endif;
						endif;
					endif;
				endif;
			endforeach;

			return NULL;
		endif;

		return NULL;
	}

	private function routePattern($url)
	{
		if ($url !== '/'):
			$newUrl = '/^'.str_replace('/', '\/', $url).'\/$/';
		else:
			$newUrl = '/^'.str_replace('/', '\/', $url).'$/';
		endif;

		preg_match_all('/(\{.*?\})/i', $url, $matches);

		$total = count($matches[0]);

		if ($total):
			for($m = 0; $m < $total; $m++):
				$pattern = '([^\/]+)';
				$newUrl = str_replace($matches[0][$m], $pattern, $newUrl);
			endfor;
		endif;

		return $newUrl;
	}

	public function getParams()
	{
		return $this->params;
	}

	public function offsetSet($offset, $value)
	{
		if (is_null($offset)):
			$this->routes[] = $value;
		else:
			$this->routes[$offset] = $value;
		endif;
	}

	public function offsetExists($offset)
	{
		return isset($this->routes[$offset]);
	}

	public function offsetUnset($offset)
	{
		unset($this->routes[$offset]);
	}

	public function offsetGet($offset)
	{
		return isset($this->routes[$offset]) ? $this->routes[$offset] : null;
	}

	public function getIterator()
	{
		return new ArrayIterator($this->routes);
	}
}
