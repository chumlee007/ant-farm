<?php

namespace AntFarm\AntFarm\Router;

use AntFarm\AntFarm\AntFarm;
use AntFarm\AntFarm\Helper;

class Input
{
	private static $session = NULL;
	private static $app = NULL;

	public function __construct(AntFarm $app)
	{
		static::$app = $app;
		static::$session = $app::$props['session'];
	}

	public static function getPost()
	{
		$rawPhpInput = file_get_contents("php://input");
		$phpInput = (!Helper::isJson($rawPhpInput)) ? $rawPhpInput : json_decode($rawPhpInput, TRUE);

		return $phpInput;
	}

	public static function get($key = NULL)
	{
		$rawPhpInput = file_get_contents("php://input");
		$phpInput = (!Helper::isJson($rawPhpInput)) ? $rawPhpInput : json_decode($rawPhpInput, TRUE);
		
		$_PUT = [];
		$_PATCH = [];
		
		$requestMethod = strtolower(static::$app->requestMethod());
		$contentType = static::$app->contentType();

		if ($requestMethod === 'post'):
			$_POST = (!Helper::isJson($rawPhpInput)) ? $_POST : $phpInput;
		endif;

		if ($requestMethod === 'put'):
			$putInput = $phpInput;

			preg_match('/boundary=(.*)$/', $contentType, $matches);

			if (count($matches)):
				$_PUT = static::parseHttpRequest($putInput);
			else:
				parse_str($putInput, $_PUT);
			endif;
		endif;

		if ($requestMethod === 'patch'):
			$patchInput = $phpInput;

			preg_match('/boundary=(.*)$/', $contentType, $matches);

			if (count($matches)):
				$_PATCH = static::parseHttpRequest($patchInput);
			else:
				parse_str($patchInput, $_PATCH);
			endif;
		endif;

		$postValue = isset($_POST[$key]) ? $_POST[$key] : NULL ;
		$putValue = isset($_PUT[$key]) ? $_PUT[$key] : NULL ;
		$patchValue = isset($_PATCH[$key]) ? $_PATCH[$key] : NULL ;
		$getValue = isset($_GET[$key]) ? $_GET[$key] : NULL ;
		$flashValue = static::$session->getFlashData($key);

		if (!$key):
			if ($requestMethod === 'post'):
				return $_POST;
			elseif($requestMethod === 'get'):
				return $_GET;
			elseif($requestMethod === 'put'):
				return $_PUT;
			elseif($requestMethod === 'patch'):
				return $_PATCH;
			else:
				return NULL;
			endif;
		elseif (isset($_POST[$key])):
			return $postValue;
		elseif(isset($_PUT[$key])):
			return $putValue;
		elseif(isset($_PATCH[$key])):
			return $patchValue;
		elseif(isset($_GET[$key])):
			return $getValue;
		elseif($flashValue):
			return $flashValue;
		else:
			return NULL;
		endif;
	}

    public static function getFile($key)
    {
        if(isset($_FILES[$key])):
            return $_FILES[$key];
        else:
            return NULL;
        endif;
    }

	public static function getAjaxFile()
    {
        return file_get_contents("php://input");
    }

	public static function old($key)
	{
		$flashData =  static::$session->getAllFlashData();
		$oldInput = isset($flashData['oldInput']) ? $flashData['oldInput'] : NULL ;

		if ($oldInput):
			if (isset($oldInput[$key])):
				return $oldInput[$key];
			endif;
		endif;

		return NULL;
	}

	public static function flash()
	{
		static::$session->flashOldInput();
	}

	private static function parseHttpRequest($input)
	{
		$contentType = static::$app->contentType();

		preg_match('/boundary=(.*)$/', $contentType, $matches);

		$boundary = $matches[1];

		$payload = [];
		$blocks = preg_split("/-+$boundary/", $input);

		array_pop($blocks);

		// loop data blocks
		foreach ($blocks as $id => $block):
			if (empty($block)) continue;

			if (strpos($block, 'application/octet-stream') !== FALSE): // parse uploaded files
				// match "name", then everything after "stream" (optional) except for prepending newlines
				preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
			
				$payload['files'][$matches[1]] = $matches[2];
			else: // parse all other fields
				// match "name" and optional value in between newline sequences
				preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
				$payload[$matches[1]] = $matches[2];
			endif;
		endforeach;

		return $payload;
	}
}