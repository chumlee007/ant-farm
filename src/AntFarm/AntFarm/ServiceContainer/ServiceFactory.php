<?php

namespace AntFarm\AntFarm\ServiceContainer;

use AntFarm\AntFarm\AntFarm;
use AntFarm\AntFarm\Environment\Environment;
use AntFarm\AntFarm\Router\Router;
use AntFarm\AntFarm\Router\Input;
use AntFarm\AntFarm\Views\View;
use AntFarm\AntFarm\Controllers\Controller;
use AntFarm\AntFarm\Session\Session;
use AntFarm\AntFarm\Cache\Cache;
use AntFarm\AntFarm\Cache\Minify;
use PHPMailer\PHPMailer\PHPMailer;

class ServiceFactory extends AbstractServiceContainer
{
	/**
	 * Stores registered services
	 * @var object[]
	 */
	private $app;
	
	public function __construct(AntFarm $app)
	{
		$this->app = $app;
	}

	protected function create_environment()
	{
		return new Environment();
	}

	protected function create_router()
	{
		return new Router($this->app);
	}

	protected function create_input()
	{
		return new Input($this->app);
	}

	protected function create_view()
	{
		return new View($this->app);
	}

	protected function create_controller()
	{
		return new Controller();
	}

	protected function create_session()
	{
		return new Session($this->app);
	}

    protected function create_cache()
    {
        return new Cache($this->app);
    }

    protected function create_minify()
    {
        return new Minify($this->app);
    }
    
    protected function create_mail()
    {
        $mail = new PHPMailer();
        
        $mail->isSMTP();
        $mail->Host = getenv('MAIL_HOST');
        $mail->SMTPAuth = true;
        $mail->Username = getenv('MAIL_USERNAME');
        $mail->Password = getenv('MAIL_PASSWORD');
        $mail->SMTPSecure = getenv('MAIL_ENCRYPTION');
        $mail->Port = (int) getenv('MAIL_PORT');
        
        return $mail;
    }
}
