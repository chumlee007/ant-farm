<?php

namespace AntFarm\AntFarm\ServiceContainer;

abstract class AbstractServiceContainer
{
	/**
	 * Stores registered services
	 * @var object[]
	 */
	private $services = array();
	
	/**
	 * Magic method that is trigered when someone calls $container->$name;
	 * @param string $name The machine name of the service. It must be a valid PHP identifier.
	 * @return object
	 */
	public function __get($name)
	{
		return isset($this->services[$name]) ? $this->services[$name] : $this->services[$name] = $this->createService($name);
	}

	/**
	 * Method to be implemented bu subclass
	 * @param string $name 
	 * @return object The service
	 * @throws Exception
	 */
	private function createService($name)
	{
		$method = 'create_'.$name;

		if (!method_exists($this, $method))
			throw new \Exception("Unknown service '$name'.");

		return $this->$method();
	}
}
