<?php

    namespace AntFarm\AntFarm\Cache;

    use AntFarm\AntFarm\AntFarm;

    class Cache
    {
        private static $app = NULL;

        public function __construct(AntFarm $app)
        {
            static::$app = $app;
        }
    }