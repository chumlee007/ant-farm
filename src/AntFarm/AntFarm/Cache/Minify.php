<?php
    namespace AntFarm\AntFarm\Cache;

    use AntFarm\AntFarm\AntFarm;
    use Assetic\Asset\AssetCollection;
    use Assetic\Asset\FileAsset;
    use Assetic\Asset\AssetCache;
    use Assetic\Asset\HttpAsset;
    use Assetic\Cache\FilesystemCache;
    use Assetic\AssetManager;
    use Assetic\AssetWriter;
    use Assetic\Filter\CssRewriteFilter;
    use Assetic\Filter\CssMinFilter;
    use Assetic\Filter\JSMinFilter;

    class Minify
    {
        private static $app = NULL;

        public function __construct(AntFarm $app)
        {
            static::$app = $app;
        }

        /**
         * Minify CSS files
         * @param string $filename Filename to be used when creating minified file
         * @param $config Configuration array:
         * <code>
         * string $config['urlToCssFolder'] Absolute url to CSS folder
         * string $config['urlToImages'] Absolute URL to where images contained in CSS files are stored
         * boolean $config['inline'] If set to TRUE, CSS styles will be included inline
         * string $config['pathToAssets'] Absolute path to folder on system that contains CSS files to be minified
         * string $config['cacheFolder'] Cache folder that is used to track changes
         * </code>
         * @param array $list An array of relative paths to CSS files that need to be minified
         */
        public static function cssPublic($filename, $config, $list)
        {
            $cdnUrl = $config['urlToCssFolder'];
            $imageUrl = isset($config['urlToImages']) ? $config['urlToImages'] : NULL ;
            $include = isset($config['include']) ? $config['include'] : TRUE;
            $inline = isset($config['inline']) ? $config['inline'] : FALSE;
            $inBrowser = isset($config['inBrowser']) ? $config['inBrowser'] : TRUE;
            $useMinifiedFile = isset($config['useMinifiedFile']) ? $config['useMinifiedFile'] : TRUE;
            $preventCaching = isset($config['preventCaching']) ? $config['preventCaching'] : FALSE;
            $baseUrl = static::$app->baseUrl();
            $basePath = $config['pathToAssets'];
            $cache = $config['cacheFolder'];
            $rawCss = [];

            $filters = array(
                new CssMinFilter(),
                new CssRewriteFilter(),
            );

            if (isset($_SERVER['HTTP_USER_AGENT'])):
                $isOldIE = preg_match('/(?i)msie [4-9]/',$_SERVER['HTTP_USER_AGENT']);
            else:
                $isOldIE = NULL;
            endif;

            if ($filename && sizeof($list) && !$isOldIE):
                if (static::$app->getEnvironment() == 'local'): // Only generate minified files locally
                    $assetCollection = array();
                    $am = new AssetManager();

                    foreach($list as $file):
                        if (strpos($file, '//') === FALSE && strpos($file, 'http://') === FALSE && strpos($file, 'https://') === FALSE):
                            $fileAsset = new FileAsset($basePath.$file, $filters);
                            $rawCss[] = $cdnUrl.$file;
                        else:
                            $fileAsset = new HttpAsset($file, $filters);
                            $rawCss[] = $file;
                        endif;

                        $ac = new AssetCache($fileAsset, new FilesystemCache($cache));

                        array_push($assetCollection, $ac);
                    endforeach;

                    $cssAssets = new AssetCollection($assetCollection);

                    $cssAssets->setTargetPath($filename);
                    $am->set('styles', $cssAssets);

                    $writer = new AssetWriter($basePath);
                    $writer->writeManagerAssets($am);
                endif;

                if ($inBrowser):
                    $styleSheet = (!$preventCaching) ? $cdnUrl.$filename : $cdnUrl.$filename.'?'.time();

                    if (static::$app->getEnvironment() == 'local'):
                        $cssContents = file_get_contents($basePath.$filename);

                        if ($imageUrl):
                            $pattern = '/url\((.*?)\)/i';

                            preg_match_all($pattern, $cssContents, $search);

                            if (count($search)):
                                $urlsToReplace = $search[0];

                                foreach($urlsToReplace as $url):
                                    $newUrl = str_replace('../', $imageUrl, $url);
                                    $cssContents = str_replace($url, $newUrl, $cssContents);
                                endforeach;
                            endif;

                            foreach($list as $file):
                                $fileAsset = new FileAsset($basePath.$file, $filters);
                                $ac = new AssetCache($fileAsset, new FilesystemCache($cache));

                                array_push($assetCollection, $ac);
                            endforeach;
                        endif;

                        if ($include && $useMinifiedFile):
                            if ($inline): ?>
                                <style type="text/css"><?=$cssContents?></style>
                            <?php   else: ?>
                                <link rel="stylesheet" href="<?=$styleSheet?>" />
                                <?php
                            endif;
                        else:
                            foreach($rawCss as $file): ?>
                                <link rel="stylesheet" href="<?=$file?>" />
                            <?php endforeach;
                        endif;
                    else:
                        $cssContents = file_get_contents($basePath.$filename);

                        if ($imageUrl):
                            $pattern = '/url\((.*?)\)/i';

                            preg_match_all($pattern, $cssContents, $search);

                            if (count($search)):
                                $urlsToReplace = $search[0];

                                foreach($urlsToReplace as $url):
                                    $newUrl = str_replace('../', $imageUrl, $url);
                                    $cssContents = str_replace($url, $newUrl, $cssContents);
                                endforeach;
                            endif;
                        endif;

                        if ($include):
                            if ($inline): ?>
                                <style type="text/css"><?=$cssContents?></style>
                    <?php   else: ?>
                                <link rel="stylesheet" href="<?=$styleSheet?>" />
                    <?php
                            endif;
                        endif;
                    endif;
                endif;
            endif;
            
            if ($inBrowser):
                if ($isOldIE):
                    foreach($list as $file):
                        if (static::$app->getEnvironment() == 'local'):
                            if (strpos($file, '//') === FALSE && strpos($file, 'http://') === FALSE && strpos($file, 'https://') === FALSE):
                                $cssContents = file_get_contents($basePath.$file);
                            else:
                                $cssContents = file_get_contents($file);
                            endif;

                            if (strpos($file, '//') === FALSE && strpos($file, 'http://') === FALSE && strpos($file, 'https://') === FALSE):
                                $pattern = '/url\((.*?)\)/i';

                                preg_match_all($pattern, $cssContents, $search);

                                if (count($search)):
                                    $urlsToReplace = $search[0];

                                    foreach($urlsToReplace as $url):
                                        $newUrl = str_replace('../', $baseUrl.'assets/public/', $url);
                                        $cssContents = str_replace($url, $newUrl, $cssContents);
                                    endforeach;
                                endif;
                            endif;

                            if ($include):
                                echo '<style type="text/css">'.$cssContents.'</style>';
                            endif;
                        else:
                            if ($include):
                                if (strpos($file, '//') === FALSE && strpos($file, 'http://') === FALSE && strpos($file, 'https://') === FALSE):
                                    $styleSheet = $cdnUrl.$file;
                                else:
                                    $styleSheet = $file;
                                endif;
                        ?>
                                <link rel="stylesheet" href="<?=$styleSheet?>" />
                        <?php
                            endif;
                        endif;
                    endforeach;
                endif;
            endif;
        }

        public static function jsPublic($filename, $config, $list)
        {
            $cdnUrl = $config['urlToJsFolder'];
            $inline = isset($config['inline']) ? $config['inline'] : FALSE;
            $include = isset($config['include']) ? $config['include'] : TRUE;
            $inBrowser = isset($config['inBrowser']) ? $config['inBrowser'] : TRUE;
            $preventCaching = isset($config['preventCaching']) ? $config['preventCaching'] : FALSE;
            $basePath = $config['pathToAssets'];
            $cache = $config['cacheFolder'];

            $filters = array(
                new JSMinFilter(),
            );

            if ($filename && sizeof($list)):
                if (static::$app->getEnvironment() == 'local'): // Only generate minified files locally
                    $assetCollection = array();
                    $am = new AssetManager();

                    foreach($list as $file):
                        if (strpos($file, '//') === FALSE && strpos($file, 'http://') === FALSE && strpos($file, 'https://') === FALSE):
                            $fileAsset = new FileAsset($basePath.$file, $filters);
                        else:
                            $fileAsset = new HttpAsset($file, $filters);
                        endif;

                        $ac = new AssetCache($fileAsset, new FilesystemCache($cache));

                        array_push($assetCollection, $ac);
                    endforeach;

                    $jsAssets = new AssetCollection($assetCollection);

                    $jsAssets->setTargetPath($filename);
                    $am->set('scripts', $jsAssets);

                    $writer = new AssetWriter($basePath);
                    $writer->writeManagerAssets($am);
                endif;

                if ($inBrowser):
                    $jsContent = file_get_contents($basePath.$filename);
                    $publicScript = (!$preventCaching) ? $cdnUrl.$filename : $cdnUrl.$filename.'?'.time() ;

                    if (static::$app->getEnvironment() !== 'local'):
                        if ($include):
                            if ($inline):
                        ?>
                                <script type="text/javascript"><?=$jsContent?></script>
                    <?php   else: ?>
                                <script type="text/javascript" src="<?=$publicScript?>"></script>
                        <?php
                            endif;
                        endif;
                    else:
                        if ($include):
                            foreach($list as $file):
                                if (strpos($file, '//') === FALSE && strpos($file, 'http://') === FALSE && strpos($file, 'https://') === FALSE):
                                    $script = $cdnUrl.$file;
                                else:
                                    $script = $file;
                                endif;
                                ?>
                                <script type="text/javascript" src="<?=$script?>"></script>
                                <?php
                            endforeach;
                        else:
                        ?>
                            <script type="text/javascript" src="<?=$publicScript?>"></script>
                        <?php
                        endif;
                    endif;
                endif;
            endif;
        }
    }