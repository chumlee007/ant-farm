<?php
namespace AntFarm\AntFarm;

use AntFarm\AntFarm\Session;

class Helper
{
	
	public function __construct() 
	{
		//
	}

    public static function getClientIp()
    {
        $ipaddress = '';

        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
	
	/**
	 * Generate random token
	 * @param Int $len - Length of token
	 * @return String $token
	 */
	public static function genToken($len = 32) 
	{
		return base64_encode(openssl_random_pseudo_bytes($len).static::getClientIp());
	}
	
	/**
	 * Encrypt the given data
	 *
	 * @param mixed $data Session data to encrypt
	 * @return mixed $data Encrypted data
	 */
	public static function encrypt($key, $data) 
	{
		$ivSize = openssl_cipher_iv_length("AES-256-CBC");
		$iv = openssl_random_pseudo_bytes($ivSize);
		$data = openssl_encrypt($data, "AES-256-CBC", $key, OPENSSL_RAW_DATA, $iv);
		// add in our IV and base64 encode the data
		$data = base64_encode($iv . $data);

		return $data;
	}
	
	/**
	 * Decrypt the given session data
	 *
	 * @param mixed $data Data to decrypt
	 * @return $data Decrypted data
	 */
	public static function decrypt($key, $data) 
	{
		$data = base64_decode($data);
		$ivSize = openssl_cipher_iv_length("AES-256-CBC");
    	$iv = substr($data, 0, $ivSize);
    	$data = openssl_decrypt(substr($data, $ivSize), "AES-256-CBC", $key, OPENSSL_RAW_DATA, $iv);
		
		return $data;
	}
	
	/**
	 * Test that data type is a string
	 * @param string $string
	 * @return string
	 */
	public static function isString($string) 
	{
		if (!is_string($string)) throw new \InvalidArgumentException("Expecting parameter 0 to be a string");
		
		return $string;
	}

    /**
     * Test if a string is a json string
     * @param string $string
     * @return bool
     */
	public static function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    /**
     * Test that data type is an int
     * @param int $int
     * @return int
     */
    public static function isInt($int)
    {
        if (!is_int($int)) throw new \InvalidArgumentException("Expecting parameter 0 to be an int");

        return $int;
    }
	
	/**
	 * Determine if object is a valid callable
	 * @param  String|Function  $callable String|Function to be validated
	 * @return Function  callable
	 */
	public static function isCallable($callable) 
	{
		if (is_string($callable) && strpos($callable, '@')):
			$split = explode('@', $callable);
			$class = $split[0];
			$method = $split[1];
			$callable = function () use ($class, $method) 
			{
				static $obj = null;
				
				if ($obj === null) $obj = new $class;
				
				return call_user_func_array(array(
					$obj,
					$method
				) , func_get_args());
			};
		endif;
		
		if (is_callable($callable)) return $callable;
		
		throw new \InvalidArgumentException('Expecting parameter 1 to be callable');
	}

    /**
     * Pretty print output to a browser or console
     * @param String|Array $value To be displayed
     * @return Nothing
     */
    public static function p($value, $ajaxOnly = FALSE)
    {
		if (\App::inDebugMode()):
			ini_set('xdebug.overload_var_dump', 0);

			$backtrace = debug_backtrace();
			$id = time().'-'.substr(str_shuffle(str_repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
			$time = date("h:i:s A");
			$list = '';
			$count = 1;

			array_shift($backtrace);

			foreach($backtrace as $arr):
				if (isset($arr['file'])):
					$color = ($count % 2) ? 'rgb(230,230,230)' : 'rgb(240,240,240)';
					$file = $arr['file'];
					$line = $arr['line'];
					$list .= '<span style="font-family: Tahoma, Verdana; padding: 0 0 0 10px; color: rgb(100,100,100); font-size: 10px; display: block; line-height: 24px; background: '.$color.';"> <strong>'.$file.'</strong>, line <strong>'.$line.'</strong></span>';
					$count++;
				endif;
			endforeach;

			if (!\App::isAjax() && !$ajaxOnly):
				echo '<div id="debug-'.$id.'" class="antfarm-debug-window" style="text-align: left !important; margin: 10px; padding: 0; background: rgb(233,233,233); border: 1px solid rgb(153,153,153);">';
					echo '<pre style="text-align: left !important; font-size: 12px; font-family: Tahoma, Verdana; margin: 0; padding: 5px; line-height: 20px; overflow: auto;">';
						var_dump($value);
					echo '</pre>';
					echo '<p style="text-align: left !important; font-size: 12px; font-family: Tahoma, Verdana; margin: 0; padding: 5px; border-top: 1px dotted rgb(153,153,153); line-height: 14px; color: rgb(0,0,0);">'.\Session::getSessionStr().'</p>';
					echo '<p style="text-align: left !important; font-size: 12px; font-family: Tahoma, Verdana; margin: 0; padding: 5px; border-top: 1px dotted rgb(153,153,153); line-height: 14px; color: rgb(0,0,0);"><strong>'.$time.'</strong>: called from ... <strong>[ <a href="#" style="color: black !important;" class="antfarm-show-more" data-debug-details-id="antfarm-debug-details-'.$id.'">more</a> ]</strong></p><div id="antfarm-debug-details-'.$id.'" style="display:none;">'.$list.'</div>';
				echo '</div>';
			elseif(\App::isAjax()):
				\App::returnJson(['output' => $value, 'time' => $time, 'file' => $backtrace[0]['file'], "line" => $backtrace[0]['line']]);
			endif;
		endif;
    }
}
