<?php
    namespace AntFarm\AntFarm\Session;

    use AntFarm\AntFarm\AntFarm;

    class MemcachedSessionHandler implements \SessionHandlerInterface
    {
        public $ttl = 1800; // 30 minutes default
        protected $config = NULL;
        protected $memcached;

        public function __construct($config, AntFarm $app)
        {
            try
            {
                $memcached = $this->getMemcached();
                $servers = $config['servers'];

                $this->ttl = empty($config['timeout']) ? $this->ttl : $config['timeout'];

                if (defined('\Memcached::OPT_CLIENT_MODE') && defined('\Memcached::DYNAMIC_CLIENT_MODE'))
                    $memcached->setOption(\Memcached::OPT_CLIENT_MODE, \Memcached::DYNAMIC_CLIENT_MODE);

                // For each server in the array, we'll just extract the configuration and add
                // the server to the Memcached connection. Once we have added all of these
                // servers we'll verify the connection is successful and return it back.
                foreach ($servers as $server):
                    $memcached->addServer($server['host'], $server['port'], $server['weight']);
                endforeach;

                if ($memcached->getVersion() === false)
                    throw new \RuntimeException("Could not establish Memcached connection.");

                $this->memcached = $memcached;
            } catch(\Exception $e)
            {
                throw new \Exception("Error establishing connection to Memcached.");
            }
        }

        /**
         * Get a new Memcached instance.
         *
         * @return \Memcached
         */
        protected function getMemcached()
        {
            if (class_exists('Memcached')):
                return new \Memcached;
            else:
                throw new \Exception("Memcached class does not exist");
            endif;
        }

        public function open($savePath, $sessionName)
        {
            return TRUE;
        }

        public function close()
        {
            $this->memcached->quit();

            unset($this->memcached);

            return TRUE;
        }

        public function read($sessionStr)
        {
            $result = $this->memcached->get($sessionStr);

            return ($result) ? $result : '';
        }

        public function write($sessionStr, $value)
        {
            if ($sessionStr !== session_id()):
                return (bool) $this->memcached->set($sessionStr, $value, $this->ttl);
            endif;

            return TRUE;
        }

        public function forget($sessionStr, $value)
        {
            return (bool) $this->memcached->replace($sessionStr, $value, $this->ttl);
        }

        public function flush($sessionStr)
        {
            return (bool) $this->memcached->replace($sessionStr, [], $this->ttl);
        }

        public function regenerate()
        {
            $sessionId = $this->create_sid();

            session_id($sessionId);
        }

        public function destroy($sessionStr)
        {
            return (bool) $this->memcached->delete($sessionStr);
        }

        public function gc($maxlifetime)
        {
            return TRUE;
        }
    }
