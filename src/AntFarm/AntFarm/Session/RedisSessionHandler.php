<?php
    namespace AntFarm\AntFarm\Session;

    use AntFarm\AntFarm\AntFarm;

    class RedisSessionHandler implements \SessionHandlerInterface
    {
        public $ttl = 1800;  // 30 minutes default
        protected $config = NULL;
        protected $redis;
        protected $prefix = 'PHPSESSID:';
        protected $app = NULL;
        protected $cookieName = '';

        public function __construct($config, AntFarm $app)
        {
            $this->config = $config;

            try {
                $host = empty($this->config['host']) ? '127.0.0.1' : $this->config['host'];
                $port = empty($this->config['port']) ? '6379' : $this->config['port'];
                $timeout = empty($this->config['timeout']) ? $this->ttl : $this->config['timeout'];

                $this->redis = new \Redis();
                $this->ttl = $timeout;

                if (isset($config['persistent'])):
                    $connected = $this->redis->pconnect($host, $port, $timeout, $this->config['persistent']);
                else:
                    $connected = $this->redis->connect($host, $port, $timeout);
                endif;

                if (!empty($this->config['database'])) $this->redis->select($this->config['database']);

                if (!empty($this->config['serializer'])):
                    $serializer = \Redis::SERIALIZER_NONE;

                    if ($this->config['serializer'] === 'php'):
                        $serializer = \Redis::SERIALIZER_PHP;
                    elseif ($this->config['serializer'] === 'igbinary'):
                        if (defined('Redis::SERIALIZER_IGBINARY')):
                            $serializer = \Redis::SERIALIZER_IGBINARY;
                        else:
                            $serializer = \Redis::SERIALIZER_PHP;
                        endif;
                    endif;

                    $this->redis->setOption(\Redis::OPT_SERIALIZER, $serializer);
                endif;

                return TRUE;
            } catch (\Exception $e) {
                throw new \Exception("Error establishing connection to Redis");

                return FALSE;
            }

            $this->app = $app;
            $this->cookieName = $config['cookieName'];
        }

        public function open($savePath, $sessionName)
        {
            return TRUE;
        }

        public function close()
        {
            $this->redis->close();

            unset($this->redis);

            return TRUE;
        }

        public function read($sessionStr)
        {
            $currentData = $this->redis->get($sessionStr);

            return ($currentData) ? $currentData : '';
        }

        public function write($sessionStr, $value)
        {
            if ($sessionStr !== session_id()):
                return (bool) ($this->redis->set($sessionStr, $value) && $this->redis->expire($sessionStr, $this->ttl));
            endif;

            return TRUE;
        }

        public function forget($sessionStr, $value)
        {
            return (bool) $this->write($sessionStr, $value);
        }

        public function flush($sessionStr)
        { 
            return (bool) $this->write($sessionStr, []);
        }

        public function regenerate()
        {
            $sessionId = $this->create_sid();

            session_id($sessionId);
        }

        public function destroy($sessionStr)
        {
            return (int) $this->redis->delete($sessionStr);
        }

        public function gc($maxlifetime)
        {
            return TRUE;
        }
    }
