<?php
    namespace AntFarm\AntFarm\Session;

    use AntFarm\AntFarm\AntFarm;

    class NativeSessionHandler extends \SessionHandler
    {
        public $ttl = 1800;  // 30 minutes default
        protected $sessionPath = NULL;
        protected $prefix = '';

        public function __construct($config, AntFarm $app)
        {
            $this->ttl = empty($config['timeout']) ? $this->ttl : $config['timeout'];
            $this->sessionPath = ini_get('session.save_path');
            $this->prefix = 'sess_';
        }

        public function read($sessionStr)
        {
            $file = $this->sessionPath . '/' . $this->prefix . session_id();

            if (file_exists($file)):
                $currentData = file_get_contents($file);

                return $currentData;
            else:
                return '';
            endif;
        }

        public function write($sessionStr, $value)
        {
            if ($sessionStr !== session_id()):
                $file = $this->sessionPath . '/' . $this->prefix . session_id();

                $bytes = file_put_contents($file, $value);

                return ($bytes !== FALSE) ? TRUE : FALSE;
            endif;

            return TRUE;
        }

        public function forget($sessionStr, $value)
        {
            $file = $this->sessionPath . '/' . $this->prefix . session_id();
            $bytes = file_put_contents($file, $value);

            return ($bytes !== FALSE) ? TRUE : FALSE;
        }

        public function flush()
        {
            session_unset();
        }

        public function regenerate()
        {
            $sessionId = $this->create_sid();

            session_id($sessionId);
        }

        public function destroy($key = NULL)
        {
            session_destroy();

            $file = $this->sessionPath . '/' . $this->prefix . session_id();

            if (file_exists($file)):
                unlink($file);
            endif;
        }

        public function close()
        {
            return $this->gc($this->ttl);
        }

        public function gc($maxlifetime)
        {
            $path = $this->sessionPath . '/*';

            foreach (glob($path) as $file):
                if (@filemtime($file) + $maxlifetime < time() && file_exists($file)):
                    @unlink($file);
                endif;
            endforeach;

            return TRUE;
        }
    }