<?php

    namespace AntFarm\AntFarm\Session;

    use AntFarm\AntFarm\AntFarm;
    use AntFarm\AntFarm\Helper;
    use AntFarm\AntFarm\Router\Input;

    class Session
    {
        private static $sessionHandler = NULL;
        private static $sessionConfig = NULL;
        private static $encryptionKey = NULL;
        private static $cookieName = NULL;
        private static $prefix = 'PHPSESSID:';
        private static $flashDataKeys = array();
        private static $sessionId = NULL;
        private static $sessionStr = '';
        private static $flashPrefix = 'antFarm.flash';

        public function __construct(AntFarm $app)
        {
            static::$sessionConfig = $app::getConfigItem('session');

            if (static::$sessionConfig && !$app::runningInConsole()):
                if (!array_key_exists('cookieName', static::$sessionConfig))
                    throw new \Exception("Cookie name is required");

                if (!array_key_exists('encryptionKey', static::$sessionConfig))
                    throw new \Exception("Encryption Key is required");

                $driver = (isset(static::$sessionConfig['driver'])) ? static::$sessionConfig['driver'] : 'native';

                static::$encryptionKey = static::$sessionConfig['encryptionKey'];
                static::$cookieName = static::$sessionConfig['cookieName'];

                switch($driver):
                    case "memcached":
                        if (!extension_loaded('memcached'))
                            throw new \RuntimeException("Memcached extension is required.");

                        if (!array_key_exists('servers', static::$sessionConfig))
                            throw new \Exception("Memcached servers are required");

                        static::$sessionHandler = new MemcachedSessionHandler(static::$sessionConfig, $app);

                        break;

                    case "redis":
                        if (!extension_loaded('redis'))
                            throw new \RuntimeException("Redis extension is required.");

                        if (!array_key_exists('host', static::$sessionConfig))
                            throw new \Exception("Redis host is required");

                        if (!array_key_exists('port', static::$sessionConfig))
                            throw new \Exception("Redis port is required");

                        static::$sessionHandler = new RedisSessionHandler(static::$sessionConfig, $app);

                        break;
                    case "native":
                    default:
                        ini_set('session.save_path', $app->env['storagePath'].'/sessions');
                        ini_set('session.gc_probability', 1);
                        ini_set('session.gc_divisor', 100);

                        static::$sessionHandler = new NativeSessionHandler(static::$sessionConfig, $app);
                        break;
                endswitch;

                session_set_save_handler(static::$sessionHandler);
                static::initSession();
            endif;
        }

        public static function initSession()
        {
            if (session_status() !== PHP_SESSION_ACTIVE || session_id() === '')
                session_start();
        }

        public static function sessionStarted()
        {
            return session_status() === PHP_SESSION_ACTIVE && session_id() !== '' ? TRUE : FALSE;
        }

        public static function getSessionStr()
        {
            if (static::sessionStarted()):
                return static::$cookieName . ':' . static::$prefix.session_id();
            else:
                return NULL;
            endif;
            
        }

        public static function get($key)
        {
            if (!static::sessionStarted()) static::initSession();

            $sessionStr = static::getSessionStr();

            if ($sessionStr):
                $data = static::$sessionHandler->read($sessionStr); // Get current session array

                if ($data): // Check to see if session array exists
                    $string = Helper::decrypt(static::$encryptionKey, $data); // Decrypt it
                    $arr = unserialize($string); // Unserialize data, return it back to an array

                    if ($arr):
                        if (isset($arr[$key])):
                            return $arr[$key];
                        else:
                            return NULL;
                        endif;
                    else:
                        return NULL;
                    endif;
                else:
                    return NULL;
                endif;
            else:
                return NULL;
            endif;
        }

        public static function put($key, $value)
        {
            if (!static::sessionStarted()) static::initSession();

            $temp = [];
            $sessionStr = static::getSessionStr();

            if ($sessionStr):
                $data = static::$sessionHandler->read($sessionStr); // Get current session array

                if ($data): // Check to see if session array exists
                    $string = Helper::decrypt(static::$encryptionKey, $data); // Decrypt it
                    $temp = unserialize($string); // Unserialize data, return it back to an array
                endif;

                $temp[$key] = $value; // Add/Replace value
                $newString = serialize($temp); // Serialize array
                $newData = Helper::encrypt(static::$encryptionKey, $newString); // Encrypt new data

                return static::$sessionHandler->write($sessionStr, $newData); // Write data
            endif;
        }

        public static function forget($key)
        {
            if (!static::sessionStarted()) static::initSession();

            $sessionStr = static::getSessionStr();

            if ($sessionStr):
                $data = static::$sessionHandler->read($sessionStr); // Get current session array

                if ($data): // Check to see if session array exists
                    $string = Helper::decrypt(static::$encryptionKey, $data); // Decrypt it
                    $arr = unserialize($string); // Unserialize data, return it back to an array

                    if ($arr):
                        if (isset($arr[$key])):
                            unset($arr[$key]);

                            $newString = serialize($arr); // Serialize array
                            $newData = Helper::encrypt(static::$encryptionKey, $newString); // Encrypt new data

                            return static::$sessionHandler->forget($sessionStr, $newData);
                        else:
                            throw new \Exception("$key does not exist");
                        endif;
                    else:
                        return FALSE;
                    endif;
                else:
                    return FALSE;
                endif;
            else:
                return FALSE;
            endif;
        }

        public static function flash($key, $value)
        {
            $data = static::get(static::$flashPrefix);

            if ($data):
                $temp = $data;
            else:
                $temp = [];
            endif;

            $temp[$key] = $value;

            static::put(static::$flashPrefix, $temp);
        }

        public static function flashOldInput()
        {
            $data = static::get(static::$flashPrefix);

            if ($data):
                $temp = $data;
            else:
                $temp = [];
            endif;

            $oldInput = Input::getPost();

            unset($oldInput['_antFarmCsrfToken']);

            $temp['oldInput'] = $oldInput;

            static::put(static::$flashPrefix, $temp);
        }

        public static function getAllFlashData()
        {
            $data = static::get(static::$flashPrefix);

            if ($data)
                return $data;

            return NULL;
        }

        public static function getFlashData($key)
        {
            $temp = static::get(static::$flashPrefix);
            $value = isset($temp[$key]) ? $temp[$key] : NULL ;

            if ($value):
                unset($temp[$key]);

                static::put(static::$flashPrefix, $temp);

                return $value;
            else:
                return NULL;
            endif;
        }

        public static function flushFlashData()
        {
            static::put(static::$flashPrefix, []);
        }

        public static function flush()
        {
            $sessionStr = static::getSessionStr();
            static::$sessionHandler->flush($sessionStr);
        }

        public static function destroy($key = NULL)
        {
            $sessionStr = static::getSessionStr();
            static::$sessionHandler->destroy($sessionStr);
        }

        public static function regenerate()
        {
            static::$sessionHandler->regenerate();
        }
    }