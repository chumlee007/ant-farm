<?php
    use \Monolog\Logger;
    use \Monolog\Handler\StreamHandler;

    $defaultLogger = new Logger('defaultLogger');
    $defaultLogger->pushHandler(new StreamHandler(__DIR__.'/../storage/logs/propel.log', Logger::WARNING));

    $serviceContainer->setLogger('defaultLogger', $defaultLogger);