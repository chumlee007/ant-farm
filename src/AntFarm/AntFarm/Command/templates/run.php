<?php
    require "vendor/autoload.php";

    $viewsPath = __DIR__.'/app/views';
    
    if (file_exists(__DIR__.'/.env')):
        $dotenv = new Dotenv\Dotenv(__DIR__);
        $dotenv->load();
    else:
        die('Please configure your .env file, or just rename .env.sample to .env and start from there :)');
    endif;

    if (file_exists(__DIR__.'/database/config/config.php')):
        require "database/config/config.php";
        require "database/log.php";
    endif;

    $config = [
        "debug" => filter_var(getenv("APP_DEBUG"), FILTER_VALIDATE_BOOLEAN),
        "timezone" => getenv("APP_TIMEZONE"),
        "baseUrl" => getenv("APP_URL"),
        "viewsPath" => $viewsPath,
        "publicFolder" => getenv("PUBLIC_FOLDER"),
        "environment" => getenv("APP_ENV"),
        "session" => [
            "encryptionKey" => getenv("APP_KEY"),
            "timeout" => getenv("SESSION_TIMEOUT"),
            "driver" => getenv("SESSION_DRIVER"),
            "cookieName" => getenv("SESSION_COOKIE_NAME"),
            "servers" => [
                [
                    "host" => getenv('MEMCACHED_HOST'),
                    "port" => getenv('MEMCACHED_PORT'),
                    "weight" => 1,
                ],
            ],
            "host" => getenv("REDIS_HOST"),
            "port" => getenv("REDIS_PORT"),
        ],
    ];

    $app = new AntFarm\AntFarm\AntFarm($config);

    // Routes
    $app->get("/", function() {
        App::renderView("/helloWorld.html");
    });

    $app->run();
