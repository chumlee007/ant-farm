<?php
    require "vendor/autoload.php";

    $dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
    $dotenv->load();

    return [
        'propel' => [
            'paths' => [
                'outputDir' => 'database/migrations',
                'sqlDir' => 'database/sql',
                'phpDir' => 'app/models',
                'schemaDir' => 'database',
                'phpConfDir' => 'database/config',
            ],
            'database' => [
                'connections' => [
                    'default' => [
                        'adapter' => 'mysql',
                        'classname' => 'Propel\Runtime\Connection\DebugPDO',
                        'dsn' => 'mysql:host='.getenv('DB_HOST').';dbname='.getenv('DB_DATABASE').'',
                        'user' => getenv('DB_USERNAME'),
                        'password' => getenv('DB_PASSWORD'),
                        'settings' => [
                            'charset' => 'utf8',
                            'queries' => [
                                'SET NAMES utf8 COLLATE utf8_unicode_ci, COLLATION_CONNECTION = utf8_unicode_ci, COLLATION_DATABASE = utf8_unicode_ci, COLLATION_SERVER = utf8_unicode_ci',
                            ],
                        ],
                    ]
                ]
            ],
            'runtime' => [
                'defaultConnection' => 'default',
                'connections' => ['default']
            ],
            'generator' => [
                'defaultConnection' => 'default',
                'connections' => ['default']
            ]
        ]
    ];