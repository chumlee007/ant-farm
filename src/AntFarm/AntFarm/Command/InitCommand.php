<?php
    namespace AntFarm\AntFarm\Command;

    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Input\InputOption;
    use Symfony\Component\Console\Input\ArrayInput;
    use Symfony\Component\Console\Input\StringInput;
    use Symfony\Component\Console\Output\Output;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Finder;
    use Symfony\Component\Filesystem\Exception\IOException;
    use Symfony\Component\Filesystem\Filesystem;
    use Symfony\Component\Process\Process;
    use Symfony\Component\Process\Exception\ProcessFailedException;

    class InitCommand extends Command
    {
        private $appFolder = 'app';
        private $appConfigFolder = 'config';
        private $appControllersFolder = 'app/controllers';
        private $appMiddlewareFolder = 'app/middleware';
        private $appModelsFolder = 'app/models';
        private $appViewsFolder = 'app/views';

        private $publicFolder = 'public';
        private $databaseFolder = 'database';
        private $databaseConfigFolder = 'database/config';
        private $databaseMigrationsFolder = 'database/migrations';
        private $databaseSqlFolder = 'database/sql';

        private $storageFolder = 'storage';
        private $sessionsFolder = 'storage/sessions';
        private $cacheFolder = 'storage/cache';
        private $logsFolder = 'storage/logs';

        public function __construct($name = NULL)
        {
            parent::__construct($name);
        }

        protected function configure()
        {
            parent::configure();

            $this
                ->setName('init')
                ->setDescription('Setup and configure application');
        }

        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $fs = new Filesystem();

            $output->writeln('<info>AntFarm setup ...</info>');

            // APP
            if (!$fs->exists($this->appFolder)):
                $fs->mkdir($this->appFolder);
            else:
                $output->writeln("<comment>[ $this->appFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->appConfigFolder)):
                $fs->mkdir($this->appConfigFolder);
            else:
                $output->writeln("<comment>[ $this->appConfigFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->appControllersFolder)):
                $fs->mkdir($this->appControllersFolder);
            else:
                $output->writeln("<comment>[ $this->appControllersFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->appMiddlewareFolder)):
                $fs->mkdir($this->appMiddlewareFolder);
            else:
                $output->writeln("<comment>[ $this->appMiddlewareFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->appModelsFolder)):
                $fs->mkdir($this->appModelsFolder);
                $gitignore = file_get_contents(__DIR__ . '/templates/gitignore.txt');
                $fs->dumpFile($this->appModelsFolder.'/.gitignore', $gitignore);
            else:
                $output->writeln("<comment>[ $this->appModelsFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->appViewsFolder)):
                $fs->mkdir($this->appViewsFolder);
            else:
                $output->writeln("<comment>[ $this->appViewsFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->appViewsFolder.'/helloWorld.html')):
                $html = file_get_contents(__DIR__ . '/templates/helloWorld.html');
                $fs->dumpFile($this->appViewsFolder.'/helloWorld.html', $html);
            endif;

            if (!$fs->exists($this->appConfigFolder.'/propel.php.sample')):
                $propel = file_get_contents(__DIR__ . '/templates/propel.sample.php');
                $fs->dumpFile($this->appConfigFolder.'/propel.php', $propel);
            endif;

            // PUBLIC
            $output->writeln("<info>Creating folder [ $this->publicFolder ] ...</info>");

            if (!$fs->exists($this->publicFolder)):
                $fs->mkdir($this->publicFolder);
            else:
                $output->writeln("<comment>[ $this->publicFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->publicFolder.'/.htaccess')):
                $htaccess = file_get_contents(__DIR__ . '/templates/htaccess.sample');
                $fs->dumpFile($this->publicFolder.'/.htaccess', $htaccess);
            else:
                $output->writeln("<comment>[ .htaccess ] already exists ... Skipping ...<comment>");
            endif;

            if (!$fs->exists($this->publicFolder.'/index.php')):
                $index = file_get_contents(__DIR__ . '/templates/index.php');
                $fs->dumpFile($this->publicFolder.'/index.php', $index);
            else:
                $output->writeln("<comment>[ index.php ] already exists ... Skipping ...<comment>");
            endif;

            if (!$fs->exists('run.php')):
                $run = file_get_contents(__DIR__ . '/templates/run.php');
                $fs->dumpFile('run.php', $run);
            else:
                $output->writeln("<comment>[ run.php ] already exists ... Skipping ...<comment>");
            endif;

            if (!$fs->exists('.env.sample')):
                $env = file_get_contents(__DIR__ . '/templates/.env.sample');
                $fs->dumpFile('.env', $env);
            else:
                $output->writeln("<comment>[ .env ] already exists ... Skipping ...<comment>");
            endif;

            if (!$fs->exists('README.md')):
                $readMePath = realpath(__DIR__ . '/../../../../README.md');

                if ($readMePath):
                    $readme = file_get_contents($readMePath);
                    $fs->dumpFile('README.md', $readme);
                endif;
            else:
                $output->writeln("<comment>[ README.md ] already exists ... Skipping ...<comment>");
            endif;

            if (!$fs->exists('LICENSE.txt')):
                $licensePath = realpath(__DIR__ . '/../../../../LICENSE.txt');

                if ($licensePath):
                    $license = file_get_contents($licensePath);
                    $fs->dumpFile('LICENSE.txt', $license);
                endif;
            else:
                $output->writeln("<comment>[ LICENSE.txt ] already exists ... Skipping ...<comment>");
            endif;

            // DATABASE
            $output->writeln("<info>Creating folder [ $this->databaseFolder ] ...</info>");

            if (!$fs->exists($this->databaseFolder)):
                $fs->mkdir($this->databaseFolder);
            else:
                $output->writeln("<comment>[ $this->databaseFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->databaseConfigFolder)):
                $fs->mkdir($this->databaseConfigFolder);
                $gitignore = file_get_contents(__DIR__ . '/templates/gitignore.txt');
                $fs->dumpFile($this->databaseConfigFolder.'/.gitignore', $gitignore);
            else:
                $output->writeln("<comment>[ $this->databaseConfigFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->databaseMigrationsFolder)):
                $fs->mkdir($this->databaseMigrationsFolder);
            else:
                $output->writeln("<comment>[ $this->databaseMigrationsFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->databaseSqlFolder)):
                $fs->mkdir($this->databaseSqlFolder);
                $gitignore = file_get_contents(__DIR__ . '/templates/gitignore.txt');
                $fs->dumpFile($this->databaseSqlFolder.'/.gitignore', $gitignore);
            else:
                $output->writeln("<comment>[ $this->databaseSqlFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists('../../'.$this->databaseFolder.'/schema.xml.sample')):
                $schemaXml = file_get_contents(__DIR__ . '/templates/schema.xml.sample');
                $fs->dumpFile($this->databaseFolder.'/schema.xml.sample', $schemaXml);
            endif;

            if (!$fs->exists('../../'.$this->databaseFolder.'/log.php')):
                $propelLog = file_get_contents(__DIR__ . '/templates/log.php');
                $fs->dumpFile($this->databaseFolder.'/log.php', $propelLog);
            endif;

            // STORAGE
            $output->writeln("<info>Creating folder [ $this->storageFolder ] ...</info>");

            if (!$fs->exists($this->storageFolder)):
                $fs->mkdir($this->storageFolder);
            else:
                $output->writeln("<comment>[ $this->storageFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->sessionsFolder)):
                $fs->mkdir($this->sessionsFolder);
                $gitignore = file_get_contents(__DIR__ . '/templates/gitignore.txt');
                $fs->dumpFile($this->sessionsFolder.'/.gitignore', $gitignore);
            else:
                $output->writeln("<comment>[ $this->sessionsFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->cacheFolder)):
                $fs->mkdir($this->cacheFolder);
                $gitignore = file_get_contents(__DIR__ . '/templates/gitignore.txt');
                $fs->dumpFile($this->cacheFolder.'/.gitignore', $gitignore);
            else:
                $output->writeln("<comment>[ $this->cacheFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists($this->logsFolder)):
                $fs->mkdir($this->logsFolder);

                $gitignore = file_get_contents(__DIR__ . '/templates/gitignore.txt');

                $fs->dumpFile($this->logsFolder.'/.gitignore', $gitignore);
                $fs->dumpFile($this->logsFolder.'/propel.log', '');
            else:
                $output->writeln("<comment>[ $this->logsFolder ] already exists ... Skipping ...</comment>");
            endif;

            if (!$fs->exists('.gitignore')):
                $data = ".DS_Store
.env
.env.sample
propel
queen
vendor/
public/.htaccess
composer.phar
composer.lock
Thumbs.db";

                $fs->dumpFile(".gitignore", $data);
            endif;

            // SYMLINKS
            if (!$fs->exists('propel')):
                if ($fs->exists('vendor/bin/propel')):
                    try {
                        $fs->symlink('vendor/bin/propel', 'propel');
                    } catch (\Exception$e) {
                        $output->writeln("<comment>Unable to create symlink to from 'vendor/bin/propel' to 'propel'</comment>");
                    }
                endif;
            endif;

            if (!$fs->exists('queen')):
                if ($fs->exists('vendor/bin/queen')):
                    try {
                        $fs->symlink('vendor/bin/queen', 'queen');
                    } catch (\Exception$e) {
                        $output->writeln("<comment>Unable to create symlink to from 'vendor/bin/queen' to 'queen'</comment>");
                    }
                endif;
            endif;

            $output->writeln("<info>Setup complete ...</info>");
        }
    }
